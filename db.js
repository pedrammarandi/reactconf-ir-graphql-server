module.exports = {
  posts: [
    { id: 1, title: "Iran React Conf 2019", views: 254, author_id: 123 },
    {
      id: 2,
      title: "What is Prisma.js? How does it save you money?",
      views: 65,
      author_id: 456
    },
    {
      id: 3,
      title: "How to make automatic Graphql API from your Postgres DB",
      views: 65,
      author_id: 466
    },
    {
      id: 4,
      title: "Hasura vs Graphile! Which one to choose?",
      views: 65,
      author_id: 476
    },
    {
      id: 5,
      title: "Apollo Federation is here to end your headaches",
      views: 65,
      author_id: 476
    }
  ],
  authors: [
    { id: 123, name: "Milad Heydari" },
    { id: 456, name: "Amir Tahani" },
    { id: 466, name: "Pedram Marandi" },
    { id: 476, name: "Jeremy Webster" }
  ],
  comments: [
    {
      id: 987,
      post_id: 1,
      body: "Shoooja? Koooja?",
      date: new Date("2017-07-03")
    },
    {
      id: 995,
      post_id: 1,
      body:
        "Thank you for your tutorial, we were trying to adopt these technologies but it just cancelled. I will talk to the team again.",
      date: new Date("2017-08-17")
    }
  ]
};

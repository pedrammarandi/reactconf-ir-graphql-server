const express = require('express');
const {default: jsonGraphqlExpress} = require('json-graphql-server');
const db = require('./db.js')
const cors = require('cors');


const PORT =  process.env.PORT || 8080;;
const app = express();
app.use('/graphql', jsonGraphqlExpress(db));
app.listen(PORT, () => {
    console.log("http://localhost:" + PORT + "/graphql")
}) ;

var allowedOrigins = ['http://localhost:3000/*'];
app.use(cors({
  origin: function(origin, callback){
    // allow requests with no origin 
    // (like mobile apps or curl requests)
    if(!origin) return callback(null, true);
    if(allowedOrigins.indexOf(origin) === -1){
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
}));
